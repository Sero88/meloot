package com.sero_evel.meloot;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainTransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 * Created by Sero on 1/19/2015.
 */
public class MainTransactionFragment extends Fragment {
    ImageView mImageAdd, mImageSubtract;
    TextView mBalance, mPlusMinus, textViewSelectedCat, mTransDate;
    EditText mAmount, mDescription;
    Button mSubmit;
    String mSelectionType = "no selection";
    String mCategorySelection = "none";
    Date mDate;
    Calendar calendar;
    private static int REQUEST_CATEGORY = '0';


    public static MainTransactionFragment newInstance() {
        MainTransactionFragment fragment = new MainTransactionFragment();
        Bundle args = new Bundle();
        /*args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(args);
        return fragment;
    }

    public MainTransactionFragment() {
        // Required empty public constructor
    }

    public void setBalance(String amount){
        //verify that amount is greater than 0
        Log.d("amount",amount);
        if(Double.valueOf(amount) < 0){
            return;
        }
        mBalance.setText(amount);
    }

    //used for database input
    public void assignBalance(ArrayList<String> accountInfo){
        //get transaction
        String accountString =  accountInfo.get(0);
        String[] account = accountString.split("~");
        setBalance(account[2]);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get Date
        mDate = new Date();

        //Calendar
        calendar = Calendar.getInstance();
        calendar.setTime(mDate);

        String[] columns = {
                DatabaseContract.COL_ACCOUNT_ID,
                DatabaseContract.COL_ACCOUNT_NAME,
                DatabaseContract.COL_ACCOUNT_BALANCE,};

        AsyncTaskDatabase getData = new AsyncTaskDatabase(getActivity().getApplicationContext(),DatabaseContract.TABLE_ACCOUNT,columns,"mtf",this, null);
        getData.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.fragment_main_transaction, container, false);

       //get Views
        mBalance = (TextView) v.findViewById(R.id.balance_total);
        mTransDate = (TextView)v.findViewById(R.id.transaction_date);
        mImageAdd = (ImageView)v.findViewById(R.id.imageView_plus);
        mImageSubtract = (ImageView)v.findViewById(R.id.imageView_minus);
        mPlusMinus = (TextView)v.findViewById(R.id.textView_plusMinus);
        textViewSelectedCat = (TextView)v.findViewById(R.id.textView_selected_category);

        mAmount = (EditText) v.findViewById(R.id.editText_amount);
        mDescription = (EditText)v.findViewById(R.id.editText_description);
        mSubmit = (Button)v.findViewById(R.id.button_submit);



        //set date
        mTransDate.setText(String.valueOf(mDate));

        mImageAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //switch image to button on
                mImageAdd.setImageResource(R.drawable.button_plus_on);

                //turn off the minus if is on
                if(mSelectionType.equals(DatabaseContract.TRANS_TYPE_W)){
                    mImageSubtract.setImageResource(R.drawable.button_minus_off);
                }

                //set color and + sign
                mPlusMinus.setTextColor(getResources().getColor(R.color.greenPlus));
                mPlusMinus.setText("+");

                //set selection to Deposit
                mSelectionType = DatabaseContract.TRANS_TYPE_D;

                CategoryPickerFragment dialog =  CategoryPickerFragment.newInstance(mCategorySelection,DatabaseContract.COL_CATS_D);
                FragmentManager fm = getActivity().getFragmentManager();
                dialog.setTargetFragment(MainTransactionFragment.this,REQUEST_CATEGORY);
                dialog.show(fm, "Categories");

            }
        });

        mImageSubtract.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //switch image to button on
                mImageSubtract.setImageResource(R.drawable.button_minus_on);
                if(mSelectionType.equals(DatabaseContract.TRANS_TYPE_D)){
                    mImageAdd.setImageResource(R.drawable.button_plus_off);
                }

                //set color and - sign
                mPlusMinus.setTextColor(getResources().getColor(R.color.redMinus));
                mPlusMinus.setText("-");

                //set selection to Deposit
                mSelectionType = DatabaseContract.TRANS_TYPE_W;

                //create dialog and add it to the fragment manager to display it.
                CategoryPickerFragment dialog =  CategoryPickerFragment.newInstance(mCategorySelection,DatabaseContract.COL_CATS_W);
                FragmentManager fm = getActivity().getFragmentManager();
                dialog.setTargetFragment(MainTransactionFragment.this,REQUEST_CATEGORY);
                dialog.show(fm, "Categories");


            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                double updatedBalance;
                //verify a type of transaction has been set and a category (gives error message)
                if (mCategorySelection.equals("none")) {
                    MessageDialog md = MessageDialog.newInstance("ERROR","Are you adding or subtracting? You haven't set a category!");
                    md.show(getFragmentManager(),"ERROR_NO_CAT");
                    return;
                }

                //check to see that user input a number
                String transAmountString = mAmount.getText().toString();
                if (transAmountString.matches("")){
                    MessageDialog md2 = MessageDialog.newInstance("Error: No Amount","You have not set an amount yet!");
                    md2.show(getFragmentManager(),"ERROR_NO_AMOUNT");
                    return;
                }

                //since amount is not null convert to double
                Double transAmountDouble =  Double.valueOf(transAmountString);

                //check to see if it is greater than 0
                if(transAmountDouble<=0){
                    MessageDialog md3 = MessageDialog.newInstance("Error: Incorrect Amount","The amount must be greater than 0");
                    md3.show(getFragmentManager(),"ERROR_INCORRECT_AMOUNT");
                    return;
                }



               /* GregorianCalendar nDate = new GregorianCalendar(year, month, day, hour, minute);
                Date nDate2 = nDate.getTime();
                mTransDate.setText(nDate2.toString());*/

                DatabaseHelper dbHelper = new DatabaseHelper(getActivity());

                //get the description from user
                String description = mDescription.getText().toString();
                if(description.matches("")) {
                    description = "No Description";
                }

                //get category id
                int catId = dbHelper.getCatId(mCategorySelection);
                Log.d("catID", String.valueOf(catId));

                // code to delete database (testing-only)
                // getActivity().getApplicationContext().deleteDatabase(DatabaseContract.DATABASE_MELOOT);

                //get database cat id

                //insert amount into database (will check if trans amount is trying to exceed balance)
                updatedBalance = dbHelper.calculateAmount(mSelectionType,transAmountDouble, Double.valueOf(mBalance.getText().toString()),1,getActivity());

                //insert transaction into database
                dbHelper.insertTransaction(
                        description,
                        mDate,
                        mSelectionType,
                        transAmountDouble,
                        1,
                        catId
                );

                //update the Balance
                setBalance(String.valueOf(updatedBalance));

                //reset everything to normal
                mImageAdd.setImageResource(R.drawable.button_plus_off);
                mImageSubtract.setImageResource(R.drawable.button_minus_off);
                mCategorySelection = "none";
                textViewSelectedCat.setText("");
                mPlusMinus.setText("");
                mAmount.setText("");
                mDescription.setText("");

               //update most recent transaction fragment (for now replace fragment)
               FragmentManager fm =  getActivity().getFragmentManager();
               MostRecentFragmentList listFragment = new MostRecentFragmentList();
               fm.beginTransaction().replace(R.id.listTransHolder,listFragment).commit();
            }
        });

        return v;
    }

    /*Gets result from CategoryPickerFragment */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode != Activity.RESULT_OK) return;
        if(requestCode == REQUEST_CATEGORY){
            String cat = data.getStringExtra(CategoryPickerFragment.EXTRA_CHOSEN_CAT);
            mCategorySelection = cat;
            textViewSelectedCat.setText(cat);
        }
    }


}
