package com.sero_evel.meloot;


import android.app.ListFragment;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MostRecentFragmentList extends ListFragment {
    MostRecentListAdapter mAdapter;
    Boolean mTriggerAdapter = false;
    private ArrayList<String> mTransData = new ArrayList<String>();
    public  MostRecentFragmentList MRF = this;

    public MostRecentFragmentList() {
        // Required empty public constructor
    }

    public  void assignAdapter(ArrayList<String> transData){
        Log.d("arrayThere",transData.get(0));
        mTransData = transData;

        mAdapter = new MostRecentListAdapter(transData);
        setListAdapter(mAdapter);
        mTriggerAdapter = true;
    }

    @Override
    public void onCreate (Bundle savedInstanceBundle){
        super.onCreate(savedInstanceBundle);

        String[] columns = {
                DatabaseContract.COL_TRANS_ID,
                DatabaseContract.COL_TRANS_DESCRIPTION,
                DatabaseContract.COL_TRANS_DATE_DAY,
                DatabaseContract.COL_TRANS_DATE_MONTH,
                DatabaseContract.COL_TRANS_DATE_YEAR,
                DatabaseContract.COL_TRANS_TIME_HOUR,
                DatabaseContract.COL_TRANS_TIME_MINUTE,
                DatabaseContract.COL_TRANS_TYPE,
                DatabaseContract.COL_TRANS_AMOUNT,
                DatabaseContract.COL_TRANS_CATEGORY_ID,
                DatabaseContract.COL_TRANS_ACCOUNT_ID};

        AsyncTaskDatabase getData = new AsyncTaskDatabase(getActivity(),DatabaseContract.TABLE_TRANSACTION,columns,"mrf",this, DatabaseContract.COL_TRANS_ID + " DESC");
        getData.execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(mTriggerAdapter == true) {
            ((MostRecentListAdapter) getListAdapter()).notifyDataSetChanged();
        }
    }


    private class MostRecentListAdapter extends ArrayAdapter<String> {

        public MostRecentListAdapter(ArrayList<String> transData) {
            super(getActivity(), 0, transData);

        }

        TextView mType, mAmount, mDescription;

        @Override
        public android.view.View getView(int position, android.view.View convertView, ViewGroup parent){
            if(convertView == null){
                convertView = getActivity().getLayoutInflater().inflate(R.layout.fragment_most_recent_list,null);
                mType = (TextView)convertView.findViewById(R.id.listView_mostRecent_type);
                mAmount = (TextView)convertView.findViewById(R.id.listView_mostRecent_amount);
                mDescription = (TextView)convertView.findViewById(R.id.listView_mostRecent_description);
                convertView.setTag(new ViewHolder(mType, mAmount, mDescription));
            }
            else{
                ViewHolder vh = (ViewHolder)convertView.getTag();
                mType = vh.mType;
                mAmount = vh.mAmount;
                mDescription = vh.mDescription;
            }

            //get transaction
            String transString =  mTransData.get(position);
            String[] transaction = transString.split("~");

            //set Text for views
            Log.d("Transtype",transaction[7]);
            if(transaction[7].equals("withdrawal")){
                mType.setText("-");
            }
            else{
                mType.setText("+");
            }
            mAmount.setText(transaction[8]);
            mDescription.setText(transaction[1]);

            return convertView;
        }
    }

    private class ViewHolder{
        TextView mType, mAmount, mDescription;
        public ViewHolder(TextView mType, TextView mAmount, TextView mDescription){
            this.mType = mType;
            this.mAmount = mAmount;
            this.mDescription = mDescription;
        }
    }


}
