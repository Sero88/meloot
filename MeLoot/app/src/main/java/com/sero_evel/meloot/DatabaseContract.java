package com.sero_evel.meloot;

import android.provider.BaseColumns;

/**
 * Created by Sero on 1/15/2015.
 */
public class DatabaseContract implements BaseColumns {

    public DatabaseContract(){/*empty constructor on purpose*/}
    /*Database Info*/
    public static final String DATABASE_MELOOT = "MeLoot.db";
    public static final int DATABASE_MELOOT_VERSION = 1;

    /*Table Account*/
    public static final String TABLE_ACCOUNT = "account";
    public static final String COL_ACCOUNT_ID = "acc_id";
    public static final String COL_ACCOUNT_NAME = "name";
    public static final String COL_ACCOUNT_BALANCE = "balance";

    /*Table Categories*/
    public static final String TABLE_CATEGORIES = "category";
    public static final String COL_CATS_ID = "cat_id";
    public static final String COL_CATS_NAME = "name";

    //only used once to enter initial set up amount
    public static final String INITIAL_AMOUNT = "initial_amount";

    public static final String[] COL_CATS_W =
            {"Food","Rent","Bills","Fun","Eating Out","Donations","Loan Payment","Gas","Car","Personal","Health/Medicine","Unexpected"};
    public static final String[] COL_CATS_D =
            {"Paycheck","Gift","Other","Lottery XD"};


   /*Table Transactions*/
    public static final String TABLE_TRANSACTION = "trans";
    public static final String COL_TRANS_ID = "trans_id";
    public static final String COL_TRANS_DESCRIPTION = "description";
    public static final String COL_TRANS_DATE_DAY= "day";
    public static final String COL_TRANS_DATE_MONTH= "month";
    public static final String COL_TRANS_DATE_YEAR= "year";
    public static final String COL_TRANS_TIME_HOUR = "hour";
    public static final String COL_TRANS_TIME_MINUTE = "minute";
    public static final String COL_TRANS_TYPE = "type";
    public static final String COL_TRANS_AMOUNT = "amount";
    public static final String COL_TRANS_CATEGORY_ID = "cat_id";
    public static final String COL_TRANS_ACCOUNT_ID = "acc_id";

    public static final String TRANS_TYPE_W = "withdrawal";
    public static final String TRANS_TYPE_D = "deposit";

    /*Table Cats_Trans*/
    /*public static final String TABLE_ASSIGNMENTS = "CtAssignments";
    public static final String COL_ASSGN_ID = "id";
    public static final String COL_ASSGN_TRANS_ID = "trans_id";
    public static final String COL_ASSGN_CATS_ID = "cats_id";*/


   /* SQLite Queries*/
    public static final String SQL_CREATE_TABLE_ACCOUNT =
            "create table " + TABLE_ACCOUNT
            + "("
            + COL_ACCOUNT_ID + " integer primary key autoincrement, "
            + COL_ACCOUNT_NAME + " text, "
            + COL_ACCOUNT_BALANCE + " real "
            + ")";

    public static final String SQL_CREATE_TABLE_CATEGORIES =
            "create table " + TABLE_CATEGORIES
            + "("
            + COL_CATS_ID + " integer primary key autoincrement, "
            + COL_CATS_NAME + " text "
            + ")";

    public static final String SQL_CREATE_TABLE_TRANSACTION =
            "create table " + TABLE_TRANSACTION
            + "("
            + COL_TRANS_ID + " integer primary key autoincrement, "
            + COL_TRANS_DESCRIPTION + " text, "
            + COL_TRANS_DATE_DAY + " integer, "
            + COL_TRANS_DATE_MONTH + " integer, "
            + COL_TRANS_DATE_YEAR + " integer, "
            + COL_TRANS_TIME_HOUR + " integer, "
            + COL_TRANS_TIME_MINUTE + " integer, "
            + COL_TRANS_TYPE + " text, "
            + COL_TRANS_AMOUNT + " real, "
            + COL_TRANS_ACCOUNT_ID  + " references " + TABLE_ACCOUNT + "(" + COL_ACCOUNT_ID + "), "
            + COL_TRANS_CATEGORY_ID + " references " + TABLE_CATEGORIES + "(" + COL_CATS_ID + ") "
            + ")";

   /* public static final String SQL_CREATE_TABLE_ASSIGNMENTS =
            "craete table" + TABLE_ASSIGNMENTS
            + "("
            + COL_ASSGN_ID + " integer primary key autoincrement, "
            + COL_ASSGN_TRANS_ID + " integer references " + TABLE_TRANSACTION + "(" + COL_TRANS_ID + "), "
            + COL_ASSGN_CATS_ID + " integer references " + TABLE_CATEGORIES + "(" + COL_CATS_ID + "), "
            + ")";*/



}
