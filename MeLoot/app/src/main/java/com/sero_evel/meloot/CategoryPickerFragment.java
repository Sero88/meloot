package com.sero_evel.meloot;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Created by Sero on 1/22/2015.
 */
public class CategoryPickerFragment extends DialogFragment {
    String[] mCategoryList;
    Spinner mSpinner;
    String mChosenCat;

    public static final String EXTRA_CHOSEN_CAT = "com.sero_evel.meloot.extra.chosen.cat";
    public static final String ARG_CHOSEN_CATEGORY = "com.sero_evel.meloot.chosen.cat";
    public static final String ARG_CHOSEN_LIST = "com.sero_evel.meloot.chosen.list";


    public static CategoryPickerFragment newInstance(String category, String[] list){
        Bundle args = new Bundle();
        args.putString(ARG_CHOSEN_CATEGORY, category);
        args.putStringArray(ARG_CHOSEN_LIST, list);

        CategoryPickerFragment fragment = new CategoryPickerFragment();
        fragment.setArguments(args);

        return fragment;

    }
    public CategoryPickerFragment(){

    }

    private void sendSelectedCategory(int resultCode){
        if(getTargetFragment() == null)
            return;

        Intent i = new Intent();
        i.putExtra(EXTRA_CHOSEN_CAT, mChosenCat);

        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);

    }
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        mChosenCat = getArguments().getString(ARG_CHOSEN_CATEGORY);
        mCategoryList = getArguments().getStringArray(ARG_CHOSEN_LIST);

        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_category_picker_spinner, null);

        //Get the Spinner + set the mAdapter for cat list
        mSpinner = (Spinner) v.findViewById(R.id.spinner_category_selector);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,mCategoryList);
        mSpinner.setAdapter(adapter);

        // set listener to get selection from user
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mChosenCat = mCategoryList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });{

        };
        //check to see if the user has previously selected a category
        //if so select it
        for (int i=0; i<mCategoryList.length; i++){
            if(mCategoryList[i].equals(mChosenCat)){
                mSpinner.setSelection(i);
            }
        }

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Select Category")
                .setPositiveButton(
                        android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendSelectedCategory(Activity.RESULT_OK);
                            }
                        })
                .create();


    }
}
