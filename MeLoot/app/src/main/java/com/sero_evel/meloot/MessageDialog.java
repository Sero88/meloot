package com.sero_evel.meloot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by Sero on 1/23/2015.
 */
public class MessageDialog extends DialogFragment {
    public static final String ARGUMENT_MESSAGEDIALOG_TITLE = "com.sero_evel.meloot.messageDialog.title";
    public static final String ARGUMENT_MESSAGEDIALOG_MESSAGE="com.sero_evel.meloot.messageDialog.message";
    String mTitle, mMessage;


    public static MessageDialog newInstance(String aTitle, String aMessage){
        Bundle args = new Bundle();
        args.putString(ARGUMENT_MESSAGEDIALOG_TITLE, aTitle);
        args.putString(ARGUMENT_MESSAGEDIALOG_MESSAGE, aMessage);

        MessageDialog fragment = new MessageDialog();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mTitle = getArguments().getString(ARGUMENT_MESSAGEDIALOG_TITLE);
        mMessage = getArguments().getString(ARGUMENT_MESSAGEDIALOG_MESSAGE);

        return new AlertDialog.Builder(getActivity())
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton(android.R.string.ok, null)
                .create();

    }
}
