package com.sero_evel.meloot;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;


public class MainActivity extends Activity {
    public static final String SP_INITIAL_AMOUNT ="com.sero_evel.meloot.sp.fragment_initial_amount";
    public static final String SP_USER_AMOUNT ="com.sero_evel.meloot.sp.user_amount";
    public static final String SP_USER_ACCOUNT="com.sero_evel.mellot.sp.user_accountName";
    String initAmount = "none";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getFragmentManager();

        File database = getApplicationContext().getDatabasePath("MeLoot.db");

        // if database doesn't exists ask for initial amount from user
        if(!database.exists()){
            /*Gets initial amount from user*/
            Fragment fragment = fm.findFragmentById(R.id.main_container);
            if (fragment == null) {
                fragment = new InitialSetupFragment();
            }
            fm.beginTransaction()
                    .add(R.id.main_container,fragment)
                    .commit();

        }

        // show MainTransactionFragment if database has already been created
        else {
            //create fragments
            Fragment fragment = fm.findFragmentById(R.id.main_container);
            Fragment fragmentList = fm.findFragmentById(R.id.listTransHolder);

            // main transaction fragment
            if (fragment == null) {
                fragment = MainTransactionFragment.newInstance();
            }
            //most recent transaction list fragment
            if (fragmentList == null) {
                fragmentList = new MostRecentFragmentList();
            }

            //begin transaction for main trans fragment
            fm.beginTransaction()
                    .add(R.id.main_container, fragment)
                    .commit();

            //begin transaction for list fragment
            fm.beginTransaction()
                    .add(R.id.listTransHolder, fragmentList)
                    .commit();
        }
    }
}
