package com.sero_evel.meloot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Sero on 2/2/2015.
 */
public class DialogInitialAmount extends DialogFragment {
    View v;
    Double amount;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        v = getActivity().getLayoutInflater().inflate(R.layout.fragment_initial_amount, null);

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Choose Initial Amount")
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                EditText initialAmount = (EditText) v.findViewById(R.id.initial_amount);
                                Log.d("initalAmount",initialAmount.getText().toString());
                                if (initialAmount.getText().toString().matches("")) {
                                    MessageDialog md = MessageDialog.newInstance("ERROR", "Initial Amount Must Be 0 or greater");
                                    md.show(getActivity().getFragmentManager(), "ERROR_INITIAL_AMOUNT");
                                    DialogInitialAmount di = new DialogInitialAmount();
                                    di.show(getFragmentManager(),"TRY_AGAIN");
                                }
                                else if (Double.valueOf(initialAmount.getText().toString()) > 0) {
                                    amount = Double.valueOf(initialAmount.getText().toString());
                                }
                                if (amount < 0) {
                                    MessageDialog md = MessageDialog.newInstance("ERROR", "Initial Amount Must Be 0 or greater");
                                    md.show(getActivity().getFragmentManager(), "ERROR_INITIAL_AMOUNT");
                                    return;
                                } else {
                                    Context context = getActivity();
                                    SharedPreferences sp = context.getSharedPreferences(MainActivity.SP_INITIAL_AMOUNT, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(MainActivity.SP_USER_AMOUNT, String.valueOf(amount));
                                }
                            }
                        })
                .create();
    }
}
