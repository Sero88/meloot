package com.sero_evel.meloot;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sero on 1/15/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    Context mContext;
    SQLiteDatabase mDb;

    public DatabaseHelper(Context context){
        super(context, DatabaseContract.DATABASE_MELOOT, null, DatabaseContract.DATABASE_MELOOT_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mDb = db;
        db.execSQL(DatabaseContract.SQL_CREATE_TABLE_ACCOUNT);
        db.execSQL(DatabaseContract.SQL_CREATE_TABLE_CATEGORIES);
        db.execSQL(DatabaseContract.SQL_CREATE_TABLE_TRANSACTION);
        /*db.execSQL(DatabaseContract.SQL_CREATE_TABLE_ASSIGNMENTS);*/

        //gets created database


        /*get cat values ready to insert into database*/
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.COL_CATS_NAME,"initial_amount");
        db.insert(DatabaseContract.TABLE_CATEGORIES, null, values);

        for (int i = 0; i < DatabaseContract.COL_CATS_D.length; i++) {
            values.put(DatabaseContract.COL_CATS_NAME, DatabaseContract.COL_CATS_D[i] );
            db.insert(DatabaseContract.TABLE_CATEGORIES, null, values);
        }


        for (int i = 0; i < DatabaseContract.COL_CATS_W.length; i++) {
            values.put(DatabaseContract.COL_CATS_NAME, DatabaseContract.COL_CATS_W[i] );
            db.insert(DatabaseContract.TABLE_CATEGORIES, null, values);
        }

        //get initial user Values
        SharedPreferences sp = mContext.getSharedPreferences(MainActivity.SP_INITIAL_AMOUNT,Context.MODE_PRIVATE);
        String initialName = sp.getString(MainActivity.SP_USER_ACCOUNT,"initial");
        String initialBalance = sp.getString(MainActivity.SP_USER_AMOUNT,"0");
        Log.d("initialBalance2",initialBalance);

        /*gets initial account ready */
        ContentValues accountValues = new ContentValues();
        accountValues.put(DatabaseContract.COL_ACCOUNT_NAME, initialName);
        accountValues.put(DatabaseContract.COL_ACCOUNT_BALANCE, Double.valueOf(initialBalance));

        //inserts initial account
        db.insert(DatabaseContract.TABLE_ACCOUNT, null, accountValues);

        //insert first transaction initial amount
        this.insertTransaction("Initial Amount",new Date(),DatabaseContract.TRANS_TYPE_D,Double.valueOf(initialBalance),1,1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /*inserts transaction from user*/
    public long insertTransaction(String description, Date date, String type, double amount, int accountID, int catID){
        //this(description, new Date(), type, amount)// instead of defaults
        //breaks down the trans date into separate fields for database
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;//add one since jan = 0
        Log.d("month", String.valueOf(month));
        int year = calendar.get(Calendar.YEAR);

        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.COL_TRANS_DESCRIPTION, description);
        values.put(DatabaseContract.COL_TRANS_DATE_DAY, day);
        values.put(DatabaseContract.COL_TRANS_DATE_MONTH, month);
        values.put(DatabaseContract.COL_TRANS_DATE_YEAR, year);
        values.put(DatabaseContract.COL_TRANS_TIME_HOUR, hour);
        values.put(DatabaseContract.COL_TRANS_TIME_MINUTE, minute);
        values.put(DatabaseContract.COL_TRANS_TYPE, type);
        values.put(DatabaseContract.COL_TRANS_AMOUNT, amount);
        values.put(DatabaseContract.COL_TRANS_ACCOUNT_ID, accountID);
        values.put(DatabaseContract.COL_TRANS_CATEGORY_ID, catID);

        try {
            return mDb.insert(DatabaseContract.TABLE_TRANSACTION, null, values);
        }catch(NullPointerException e){
            return getWritableDatabase().insert(DatabaseContract.TABLE_TRANSACTION, null, values);
        }
    }

    /*gets category ID from Cats table */
    public int getCatId(String category){
       int id;
       //column to return from query
       String[] catCols = {DatabaseContract.COL_CATS_ID};
       //argument for where clause
       String[] catArgs = {category};
       Log.d("CatName",category);
       SQLiteDatabase db = this.getReadableDatabase();
       //todo finish writing query, test database cat entries (did it work) and transaction entries
       //distinct, table, return columns, column for where clause, values for where clause, group, filter, sort
       Cursor c = db.query(true, DatabaseContract.TABLE_CATEGORIES, catCols, DatabaseContract.COL_CATS_NAME + " = ?" ,catArgs, null, null, null,null  );
      // Cursor c = db.query(true, DatabaseContract.TABLE_CATEGORIES,null, null,null,null,null,null,null);
       /* for (int i = 0; i < c.getCount(); i++) {
              if(i == 0){
                  c.moveToFirst();
                  Log.d("CatName",c.getString(c.getColumnIndex(DatabaseContract.COL_CATS_NAME)));
              }
              else{
                  if(c.moveToNext()){
                      Log.d("CatName",c.getString(c.getColumnIndex(DatabaseContract.COL_CATS_NAME)));
                  }
              }
        }*/
       if(c.moveToFirst()){
           Log.d("DatabaseResult","move to first is true");
           id = c.getInt(c.getColumnIndex(DatabaseContract.COL_CATS_ID));
           return id;
       }

       return -1;

    }

    public double getAmount (int accountId){
        //column to return from query
        String[] selCol = {DatabaseContract.COL_ACCOUNT_BALANCE};
        //argument for where clause
        String[] whereArgs = {String.valueOf(accountId)};
        double amount;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(true, DatabaseContract.TABLE_ACCOUNT, selCol, DatabaseContract.COL_ACCOUNT_ID +" = ",whereArgs, null, null, null, null);
        if(c.moveToFirst()){
             amount = c.getDouble(c.getColumnIndex(DatabaseContract.COL_ACCOUNT_BALANCE));
             return amount;
        }

        return -1;
    }

    public double calculateAmount(String transType, Double transAmount, Double accountAmount, int accountId, Activity activity){
        //double accountAmount = getAmount(accountId);
        double newAmount = 0;
        if(accountAmount < 0){
            Log.i("GetAmountFailed","Couldn't get account balance");
            return -1;
        }

        switch (transType) {
            case "withdrawal":
                newAmount = accountAmount - transAmount;
                //check if TransAmount won't exceed
                if (newAmount < 0) {
                    MessageDialog md = MessageDialog.newInstance("ERROR", "The amount you're trying to subtract exceeds your funds!");
                    md.show(activity.getFragmentManager(), "ERROR_EXCEEDS");
                    return -1;
                }
                break;

            case "deposit":
                newAmount = accountAmount + transAmount;
                break;
        }

        ContentValues values = new ContentValues();
        values.put(DatabaseContract.COL_ACCOUNT_BALANCE, newAmount);
        String[] whereArgs = {String.valueOf(accountId)};
        getWritableDatabase().update(DatabaseContract.TABLE_ACCOUNT,values, DatabaseContract.COL_ACCOUNT_ID + " = ? ",whereArgs);
        return newAmount;
    }


/*
//     ran only once for set up
    public void insertCategories(){
        ContentValues values = new ContentValues();

        for (int i = 0; i < DatabaseContract.COL_CATS_D.length; i++) {
             values.put(DatabaseContract.COL_CATS_D[i], DatabaseContract.COL_CATS_NAME);
        }

        for (int i = 0; i < DatabaseContract.COL_CATS_W.length; i++) {
             values.put(DatabaseContract.COL_CATS_W[i], DatabaseContract.COL_CATS_NAME);
        }

        getWritableDatabase().insert(DatabaseContract.TABLE_CATEGORIES, null, values);
    }
*/



}
