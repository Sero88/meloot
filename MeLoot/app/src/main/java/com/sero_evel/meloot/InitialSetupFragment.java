package com.sero_evel.meloot;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sero_evel.meloot.R;

/**
 * Created by Sero on 2/4/2015.
 */
public class InitialSetupFragment extends Fragment {
    EditText mAccountName, mBalance;
    Button mSubmit;
    FrameLayout listHolder;
    TextView mostRecent;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_initial_amount, container,false);

        mBalance = (EditText) v.findViewById(R.id.initial_amount);
        mSubmit = (Button) v.findViewById(R.id.initial_submit);
        mAccountName = (EditText) v.findViewById(R.id.account_name);

        //hide the most_recent_transaction box and text
        listHolder = (FrameLayout)getActivity().findViewById(R.id.listTransHolder);
        mostRecent = (TextView)getActivity().findViewById(R.id.text_most_recent);
        listHolder.setVisibility(View.INVISIBLE);
        mostRecent.setVisibility(View.INVISIBLE);

        mSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //get Account Name from user
                String initialAccountName = mAccountName.getText().toString();

                //if user didn't input anything name account Initial
                if (initialAccountName.matches("")) {
                    initialAccountName = "initial";
                }

                //get balance from user
                String initialBalance = mBalance.getText().toString();
                FragmentManager fm = getActivity().getFragmentManager();

                //if balance is empty, show error and return
                if (initialBalance.matches("")) {
                    MessageDialog md = MessageDialog.newInstance("ERROR", "Starting balance must be 0 or greater!");
                    md.show(fm, "ERROR_INITIAL_AMOUNT");
                    return;
                }
                //if balance is set store name and balance on SharedPreferences to user later on Database
                //replaces fragment to MainTransactionFragment
                else {
                    SharedPreferences sp = getActivity().getApplicationContext().getSharedPreferences(MainActivity.SP_INITIAL_AMOUNT, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    Log.d("initialBalance1",initialBalance);
                    editor.putString(MainActivity.SP_USER_AMOUNT, initialBalance);
                    editor.putString(MainActivity.SP_USER_ACCOUNT, initialAccountName);
                    editor.commit();

                    //replace fragment
                    Fragment fragment = MainTransactionFragment.newInstance();
                    fm.beginTransaction()
                            .replace(R.id.main_container, fragment)
                            .commit();

                    //add fragment list
                   Fragment fragmentList = new MostRecentFragmentList();
                   //begin transaction for list fragment
                   fm.beginTransaction()
                            .add(R.id.listTransHolder, fragmentList)
                            .commit();

                    //show listHolder and text "most recent"
                    listHolder.setVisibility(View.VISIBLE);
                    mostRecent.setVisibility(View.VISIBLE);

                    //hide keyboard
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mBalance.getWindowToken(),0);
                    imm.hideSoftInputFromWindow(mAccountName.getWindowToken(),0);
                }
            }
        });
        return v;
    }
}
