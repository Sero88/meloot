package com.sero_evel.meloot;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Sero on 1/17/2015.
 */
public class AsyncTaskDatabase extends AsyncTask <ArrayList<String>, Void, ArrayList<String>>{
    ArrayList<String> databaseData = new ArrayList<String>();
    private Context mAppContext;
    private String mTable;
    private String mOrderBy;
    private String[] mColumns;
    private String dataEntry;
    private String mClassSelector;
    private MainTransactionFragment mMainTransactionFragment;
    private MostRecentFragmentList mMostRecentFragmentList;

    private void getFromCursor(Cursor c){
        //loops through columns to concatenate data grabbed from cursor
        for(int i=0; i<mColumns.length; i++){
            if(i==0){
                dataEntry = c.getString(c.getColumnIndex(mColumns[i]));
            }
            else{
                dataEntry += "~"+ c.getString(c.getColumnIndex(mColumns[i]));
            }
        }
        //adds all transaction data to ArrayList
        databaseData.add(dataEntry);

    }
    public AsyncTaskDatabase(Context appContext, String table, String[] columns,String classSelector,MostRecentFragmentList instance, String orderBy ){
        mAppContext = appContext;
        mTable = table;
        mColumns = columns;
        mClassSelector = classSelector;
        mMostRecentFragmentList = instance;
        mOrderBy = orderBy;

    }

    public AsyncTaskDatabase(Context appContext, String table, String[] columns,String classSelector,MainTransactionFragment instance,String orderBy ){
        mAppContext = appContext;
        mTable = table;
        mColumns = columns;
        mClassSelector = classSelector;
        mMainTransactionFragment = instance;
        mOrderBy = orderBy;
    }
    @Override
    protected ArrayList<String> doInBackground (ArrayList<String>... Params){

        DatabaseHelper dbHelper = new DatabaseHelper(mAppContext.getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();


        Cursor c = db.query(true, mTable, mColumns ,null, null, null, null, mOrderBy , null);
        if(c.getCount()>0) {
            c.moveToFirst();
            getFromCursor(c);

            while(c.moveToNext()){
                getFromCursor(c);
            }

        }
        return databaseData;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings){
        //check if strings has any content if not don't call methods
       // Log.d("arrayString",strings.get(0));
        if(strings.size() > 0) {
            switch (mClassSelector) {
                //mtf = most recent transaction fragment
                case "mrf":
                    mMostRecentFragmentList.assignAdapter(strings);
                    break;

                case "mtf":
                    mMainTransactionFragment.assignBalance(strings);
                    break;
            }
        }

    }

}
